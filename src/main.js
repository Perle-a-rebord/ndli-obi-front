import Vue from "vue";
import App from "./App.vue";
import interceptor from "./api/interceptor";
import VueRouter from 'vue-router';

interceptor();

Vue.config.productionTip = false;

new Vue({
  render: (h) => h(App),
}).$mount("#app");

Vue.use(VueRouter);

import Button from './components/Button.vue';
import Footer from './components/Footer.vue';
import Header from './components/Header.vue';
import darkmode from './components/darkmode.vue';
import filtres from './components/filtres.vue';
import research from './components/Research.vue'

const routes = [
    { path : '/', component : research },
    { path : '/button', component : Button},
    { path : '/Footer', component : Footer},
    { path : '/Header', component : Header},
    { path : '/darkmode', component : darkmode},
    { path : '/filtres', component : filtres},
    { path : '/Research', component : research},
  ]

const router = new VueRouter({
  routes
})

new Vue({
  router, render: h => h(App)
}).$mount('#app');
