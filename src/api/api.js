import axios from "axios";

// Override to set a default base url
export default axios.create({
  baseURL: '/api',
});
