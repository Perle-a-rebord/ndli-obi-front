import api from "./api";

export default function interceptor() {
  // Intercept outgoing request to add authorization token in header
  api.interceptors.request.use(
    (config) => {
      const token = localStorage.getItem("token");
      if (token) {
        config.headers.common["authorization"] = token;
      }
      return config;
    },
    (error) => {
      return Promise.reject(error);
    }
  );
}
